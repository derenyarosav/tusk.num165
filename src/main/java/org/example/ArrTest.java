package org.example;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class ArrTest {
    public static void printInter(Predicate<Integer> predicate, List<Integer> list) {
        int sum = 0;
        for (Integer n : list) {

            if (predicate.test(n)) {
                sum += n;
                System.out.println(n + " ");

            }
        }
        System.out.println("Сума чисел : " + sum);


    }

    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(3, 7, 9, 8, 2);
        System.out.println("Парні числа : ");
        printInter((n) -> n % 2 == 0, list);

        System.out.println("Непарні числа : ");
        printInter((n) -> n % 2 != 0, list);

    }

    // Tusk number : 4

}
